FROM openjdk:11-alpine
EXPOSE 8090
COPY build/libs/*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
